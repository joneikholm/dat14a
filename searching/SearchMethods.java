package searching;

import java.util.Arrays;

/**
 * Created by joneikholm on 09-10-15.
 */
public class SearchMethods
{
    public static void main(String[] args)
    {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++)
        {
            array[i]= i*2;
            //array[i]= (int)(Math.random()*10);
        }
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        long start = System.nanoTime();
        System.out.println(binarySearch(array, 17));

        long difference = System.nanoTime()-start;
        System.out.println("tid:"+difference/1000);



    }
    private static boolean findesElement(int[] arr, int tal){
        for (int i : arr)
        {
            if(i==tal){
                return true;
            }
        }
        return false;
    }

    public static boolean binarySearch(int[] sorteretArr, int tal){
        return binarySearch(sorteretArr,tal,0,sorteretArr.length-1);
    }

    private static boolean binarySearch(int[] sorteretArr, int tal, int start, int slut){
        if(start>slut) return false;
        int half=start+(slut-start)/2;
        System.out.println("half:"+half + " value:"+sorteretArr[half]);
        if(sorteretArr[half]==tal){
            return true;
        }else{
            if(sorteretArr[half] < tal){
                return  binarySearch(sorteretArr, tal, half+1, slut);
            }else {
                return  binarySearch(sorteretArr, tal, start , half-1);
            }
        }
    }


}
