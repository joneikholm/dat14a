package threads;

import java.util.Random;

/**
 * Created by joneikholm on 13-11-15.
 */
public class Producer implements Runnable
{
    private TicketMaster ticketMaster;
    public Producer(TicketMaster ticketMaster){
        this.ticketMaster = ticketMaster;
    }

    @Override
    public void run()
    {
        Random random = new Random();
        while (true){
            try
            {
                ticketMaster.addTicket();
                Thread.sleep(random.nextInt(2000));

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }
}
