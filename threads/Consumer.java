package threads;

import java.util.Random;

/**
 * Created by joneikholm on 13-11-15.
 */
public class Consumer implements Runnable
{

    private TicketMaster ticketMaster;
    public Consumer(TicketMaster ticketMaster){
        this.ticketMaster = ticketMaster;
    }

    @Override
    public void run()
    {
        Random random = new Random();
        while (true){
            try
            {
                ticketMaster.getTicket(); // går tomhændet hjem
                Thread.sleep(random.nextInt(4000));

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }
}
