package threads;

/**
 * Created by joneikholm on 13-11-15.
 */
public class RunExperiment
{
    public static void main(String[] args)
    {
        TicketMaster ticketMaster=new TicketMaster();
        Consumer consumer = new Consumer(ticketMaster);
        Producer producer = new Producer(ticketMaster);
        Thread consThread = new Thread(consumer);
        Thread prodThread = new Thread(producer);
        consThread.start();
        prodThread.start();

    }
}
