package threads;

/**
 * Created by joneikholm on 13-11-15.
 */
public class TicketMaster
{
    private int numberOfTickets = 1;
    private int MAXnumberOfTickets = 10;

    public synchronized int getTicket()
    {
        try
        {
            while (numberOfTickets == 0)
            {
                wait();
            }
            // Tråd1 stopper her, og Tråd2 får lov at køre
            numberOfTickets--;
            System.out.println("getTicket, new number:" + numberOfTickets);
            notifyAll();
            return 1;
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return 0;

    }


    public synchronized void addTicket()
    {
        try
        {
            while (numberOfTickets >= MAXnumberOfTickets)
            {
                wait();
            }
            numberOfTickets++;
            notifyAll();
            System.out.println("addTicket, new number:" + numberOfTickets);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }


    }
}
