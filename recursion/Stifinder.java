package recursion;

import java.io.File;

/**
 * Created by joneikholm on 06-10-15.
 */
public class Stifinder
{
    public static void main(String[] args) {
        String testpath=System.getProperty("user.dir");
        printIndhold(testpath);
        /*System.out.println(testpath);
        File file = new File(testpath+"/src");
        System.out.println(file.getName());
        if(file.isDirectory()){
            System.out.println("der er en mappe");
            for(int i=0; i<file.list().length; i++){
                System.out.println(file.list()[i]);
            }

        }else if(file.isFile()){
            System.out.println("der er en fil");
        }*/
    }
    private static void printIndhold(String path){
        printIndhold(path,"");
    }
    private static void printIndhold(String path, String space){
        File file = new File(path);
        System.out.println(space + file.getName());
        if(file.isDirectory()){
            for(int i=0; i<file.list().length; i++){
                printIndhold(file.listFiles()[i].getPath(), space+"  ");
            }
        }
    }
}
