package recursion;

/**
 * Created by joneikholm on 06-10-15.
 */
public class SimpleRecursion
{
    public static void main(String[] args) {
        System.out.println(factorial(4));
    }

    private static void printString(String string){
        if(string.length()>0){
            printString(string.substring(1));
            System.out.print(string.charAt(0));
        }
    }
    private static int factorial(int n){
        if(n==2){//base case
            return n;
        }else {// rekursiv case, sørg for at problemet bliver mindre
         return n * factorial(n - 1);  // 3 *  2 * 1
        }

    }
}
