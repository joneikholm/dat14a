package stacks;

import java.util.EmptyStackException;

/**
 * Created by joneikholm on 13-10-15.
 */

public class Stack implements MonsterStack
{

    private Object[] stack;
    private int top=-1;
    private int numberOfItems=0;
    public Stack(){
        stack = new Object[1000];
        java.util.Stack stack = new java.util.Stack();

    }

    @Override
    public void push(Object o)
    {
        top++;
        numberOfItems++;
        stack[top]=o;  //mere nørdet  stack[++top]=o;
    }

    @Override
    public Object pop()
    {
        if(!isEmpty())
        {
            numberOfItems--;
            return stack[top--];
        }else {
            throw new EmptyStackException();
        }
    }

    @Override
    public Object peek()
    {
        if(!isEmpty())
        {
            return stack[top];
        }else {
            throw new EmptyStackException();
        }
    }

    @Override
    public int getSize()
    {
        return numberOfItems;
    }

    @Override
    public boolean isEmpty()
    {
        return top==-1;
    }

    @Override
    public void clear()
    {
        numberOfItems=0;
        top=-1;
    }
}
