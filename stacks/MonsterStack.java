package stacks;

/**
 * Created by joneikholm on 13-10-15.
 */
public interface MonsterStack
{
    public void push(Object o);
    public Object pop();
    public Object peek();
    public int getSize();
    public boolean isEmpty();
    public void clear();
}
