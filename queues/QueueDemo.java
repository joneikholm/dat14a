package queues;

/**
 * Created by joneikholm on 03-11-15.
 */
public class QueueDemo
{
    public static void main(String[] args)
    {
        // ADT Abstract Data Types.
        // alle disse skal være public
        // int getSize()
        // void enqueue(int i)
        // int dequeue()
        // void createEmpty()
        // int peek()
        // skal vi have nogen private ?
        // boolean isFull()  // en løsning er at fordoble arrayet
        QueController queController=new QueController();
        queController.enQueue(12);
        queController.enQueue(1);
        queController.enQueue(2);
        System.out.println(queController.deQueue());
        System.out.println(queController.deQueue());
        System.out.println(queController.deQueue());
    }
}
