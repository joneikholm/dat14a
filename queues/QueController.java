package queues;
public class QueController {

    private int[] que = new int[5];
    public int back = 0;
    public int front = 0;
    public int size = 0;
    // all these need to be public
    // return int getSize()
    // void enQueue() - (int i) - back(skal forskydes)
    //  return int deQueue() - front
    // void createEmpty()
    // return int peek()
    // return boolean isFull() - when full. double the space of the array.

    public void checkIndhold() {
        for(int i= 0; i<que.length; i++) {
            System.out.print(que[i]);
        }
    }
    public QueController() {

    }

    public int getSize() {
        return size;
    }

    public void enQueue(int i) {
        if(isFull()){
            throw new IndexOutOfBoundsException();
        }

        que[back] = i;
        back = (back+1) % que.length;
        size++;
    }
    public int deQueue() {
        if(size == 0) {
            throw new NullPointerException();
        }
        int number = que[front];
        front = (front+1) % que.length;
        size--;
        //isFull();
        return number;
    }
    public void createEmpty() {
        front = 0;
        back = 0;
        size = 0;
    }
    public int peek() {
        int num = que[front];
        return num;
    }
    private boolean isFull() {
      return size==que.length-1;

    }

}
