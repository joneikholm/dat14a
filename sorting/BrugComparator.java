package sorting;

import java.util.Arrays;

/**
 * Created by joneikholm on 09-10-15.
 */
public class BrugComparator
{
    public static void main(String[] args)
    {
        String[] navne = new String[]{ "bent", "anna", "susanne","per"};
        System.out.println(Arrays.toString(navne));
        Arrays.sort(navne,new MyComparator());
        System.out.println(Arrays.toString(navne));

    }
}
