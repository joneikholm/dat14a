package network;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by joneikholm on 16-10-15.
 */
public class SmallTCPServer
{

    public static void main(String[] args)
    {
   new SmallTCPServer();
    }

    public SmallTCPServer(){
        ServerSocket sc=null;
        try
        {
            sc = new ServerSocket(1337);
            System.out.println("Afventer klient... ip:"+ InetAddress.getLocalHost().getHostAddress());
            while (true)
            {
                Socket socket = sc.accept();  // tager imod ny klient
                System.out.println("modtaget"+ socket.getInetAddress());
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

                while(dataInputStream.available()>0)
                    System.out.println(dataInputStream.readUTF());

                System.out.println("end of loop");
            }


        } catch (IOException e)
        {
            e.printStackTrace();
        }finally
        {
            try
            {
                sc.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }


    }
}
