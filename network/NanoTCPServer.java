package network;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by joneikholm on 16-10-15.
 */
public class NanoTCPServer
{

    public static void main(String[] args)
    {
        ServerSocket sc=null;
        try
        {
             sc = new ServerSocket(1337);
            System.out.println("Afventer klient... ip:"+ InetAddress.getLocalHost().getHostAddress());
            while (true)
            {
                Socket socket = sc.accept();  // tager imod ny klient
                InputStream is = socket.getInputStream();
                byte[] buffer = new byte[29];
                int lengthRead = is.read(buffer);
                for (int i = 0; i < lengthRead; i++)
                {
                    System.out.print((char) buffer[i]);
                }
                    System.out.println();
            }


        } catch (IOException e)
        {
            e.printStackTrace();
        }finally
        {
            try
            {
                sc.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }
}
