package network;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by joneikholm on 16-10-15.
 */
public class NanoTCPClient
{
    public static void main(String[] args)
    {

        try
        {
            Socket socket = new Socket(InetAddress.getLocalHost(), 1337);
            OutputStream os = socket.getOutputStream();
            os.write("halloJon".getBytes());  // sender et 'a'
            socket.close();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
