package testing;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joneikholm on 15/09/15.
 */
public class ChainListTest
{

    ChainList chainList;
    @Before
    public void setUp() throws Exception {
        chainList = new ChainList();
    }

    @Test
    public void testGetNumItems() throws Exception {
        assertEquals(0, chainList.getNumItems());
        chainList.add(new Node(3232));
        assertEquals(1, chainList.getNumItems());

    }

    @Test
    public void testAdd() throws Exception {
        chainList.add(new Node(2341));
       // assertNotNull(chainList.find(0));
    }

    @Test
    public void testFind() throws Exception {


    }

    @Test
    public void testAdd1() throws Exception {

    }

    @Test
    public void testToString() throws Exception {

    }
}