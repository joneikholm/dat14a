package patterns.iterator;

import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class TestIterator
{
    public static void main(String[] args)
    {
        Division division = new Division("Sales");

        division.add("Mark");
        division.add("Rasmus");
        division.add("Sandra");
        Iterator<VP> iterator = division.iterator();

        while (iterator.hasNext()){
            iterator.next().print();
        }

    }
}
