package patterns.iterator;

import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class Division
{
    private VP[] vps = new VP[100];
    private String divisionName;
    private int position=0;

    public Division(String divisionName)
    {
        this.divisionName = divisionName;
    }

    public void add(String name){
        vps[position++] = new VP(name, divisionName);
    }
    public Iterator<VP> iterator(){
        return new VPIterator(vps);
    }
}
