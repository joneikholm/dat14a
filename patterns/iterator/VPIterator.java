package patterns.iterator;

import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class VPIterator implements Iterator<VP>
{
    private VP[] vps;
    private int location=0;

    public VPIterator(VP[] vps)
    {
        this.vps = vps;
    }

    @Override
    public boolean hasNext()// er du klar at returnere én mere
    {
        if(location<vps.length && vps[location]!=null){
            return true;
        }
        return false;
    }

    @Override
    public VP next()
    {
        return vps[location++];
    }
}
