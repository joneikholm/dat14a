package patterns.strategyJ8;

import java.util.function.BinaryOperator;

/**
 * Created by joneikholm on 01-12-15.
 */
public class Context
{
    public void setBinaryOperator(BinaryOperator<Integer> binaryOperator)
    {
        this.binaryOperator = binaryOperator;
    }

    BinaryOperator<Integer> binaryOperator;

    public Context(BinaryOperator<Integer> binaryOperator)
    {
        this.binaryOperator = binaryOperator;
    }
}
