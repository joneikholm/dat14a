package patterns.strategyJ8;

/**
 * Created by joneikholm on 01-12-15.
 */
public class RunMathOperations
{
    public static void main(String[] args)
    {
        Context context = new Context(MathFunctions.add);
        System.out.println(context.binaryOperator.apply(5,6));
        context.setBinaryOperator(MathFunctions.divide);
        int result=context.binaryOperator.apply(55,6);
        System.out.println(context.binaryOperator.apply(55,result));
        context.setBinaryOperator((final Integer a, final Integer b)->{
            return a%b;
        });
        System.out.println(context.binaryOperator.apply(55,52));

    }
}
