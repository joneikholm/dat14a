package patterns.strategyJ8;

import java.util.function.BinaryOperator;

/**
 * Created by joneikholm on 01-12-15.
 */
public class MathFunctions
{
    static final BinaryOperator<Integer> add = (final Integer a, final Integer b) ->
    {
        return a+b;
    };
    static final BinaryOperator<Integer> multiply = (final Integer a, final Integer b) ->
    {
        return a*b;
    };
    static final BinaryOperator<Integer> subtract = (final Integer a, final Integer b) ->
    {
        return a-b;
    };
    static final BinaryOperator<Integer> divide = (final Integer a, final Integer b) ->
    {
        return a/b;
    };
}
