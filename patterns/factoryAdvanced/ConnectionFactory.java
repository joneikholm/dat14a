package patterns.factoryAdvanced;


public abstract class ConnectionFactory
{
  public ConnectionFactory()
  {
  }

  protected abstract Connection createConnection(String type);
}
