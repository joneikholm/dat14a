package patterns.factoryAdvanced;


public class TestFactory
{
  public static void main(String args[])
  {
    ConnectionFactory factory;  // programmør må selv sørge for at arve fra ConnectionFactory her

    factory = new SecureFactory();
    //factory = new UnSecureFactory(); 

    Connection connection = factory.createConnection("Oracle");

    System.out.println("You're connecting with " + 
      connection.description());
  }
}
