package patterns.factoryAdvanced;



public class SecureSqlServerConnection extends Connection
{
  public SecureSqlServerConnection()
  {
  }

  public String description()
  {
    return "SQL Server secure";
  }
}
