package patterns.decorator;
public class CD extends ComponentDecorator
{
  public CD(Computer c)
  {
    computer = c;
  }

  public String description()
  {
    return computer.description() + " and a CD";
  }
}
