package patterns.decorator;
public class Disk extends ComponentDecorator
{

  public Disk(Computer c)
  {
    computer = c;
  }

  public String description()
  {
    return computer.description() + " and a disk";
  }
  public String toString(){
	  return this.toString()+" our way";
  }
}
