package patterns.decorator;
public class Monitor extends ComponentDecorator
{

  public Monitor(Computer c)
  {
    computer = c;
  }

  public String description()
  {
    return computer.description() + " and a monitor";
  }
}
