package patterns.decorator;
public abstract class ComponentDecorator extends Computer
{
	Computer computer;
    public abstract String description();
}
