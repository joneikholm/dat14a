package patterns.mediator;

/**
 * Created by joneikholm on 08-12-15.
 */
public interface Component
{
    public void update(String s);
}
