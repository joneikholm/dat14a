package patterns.mediator;

import javafx.scene.control.Button;

/**
 * Created by joneikholm on 08-12-15.
 */
public class MButton extends Button implements Component
{
    private Mediator mediator;
    @Override
    public void update(String s)
    {
        if(s.equals("disable")){
            setDisable(true);
        }else {
            setDisable(false);
        }
    }

    public MButton(Mediator mediator, String name){
        super(name);
        this.mediator = mediator;
        mediator.add(this);
        //setOnAction(event -> mediator.notifyMediator(this));
    }
}
