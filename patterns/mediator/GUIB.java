package patterns.mediator;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class GUIB extends Application
{
    private VBox vBox;
    WebView webView = new WebView();
    ListView<MButton> buttonListView = new ListView<>();
    ObservableList<MButton> items = FXCollections.observableArrayList();


    public static void main(String[] args)
    {
        launch(args);
    }

    public GUIB(){
        buttonListView.setItems(items);
    }
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        BorderPane borderPane = new BorderPane();
        webView.getEngine().load("http://www.dr.dk/");
        vBox = new VBox(5);
        Mediator mediator = new ConcreteMediator();
        TextField newButtonField = new TextField();
        newButtonField.setOnAction(event -> {
            MButton button = new MButton(mediator, newButtonField.getText());
            button.setOnAction(event1 -> {
                webView.getEngine().load("http://"+newButtonField.getText()+"/");
                mediator.notifyMediator(button);
            });
            items.add(button);
        });

        CheckBox checkBox = new CheckBox("test");
        checkBox.setSelected(false);
        vBox.getChildren().addAll(checkBox,newButtonField,buttonListView);
        borderPane.setLeft(vBox);
        borderPane.setCenter(webView);
        Scene scene = new Scene(borderPane, 900, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}