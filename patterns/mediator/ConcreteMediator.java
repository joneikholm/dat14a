package patterns.mediator;

import java.util.ArrayList;

/**
 * Created by joneikholm on 08-12-15.
 */
public class ConcreteMediator implements Mediator
{

    ArrayList<Component> list = new ArrayList<>();

    @Override
    public void notifyAllClients(Component button)
    {
        for (Component component : list)
        {
            if(component.equals(button)){
                component.update("disable");
            }else{
                component.update("enable");
            }
        }
    }

    @Override
    public void add(Component button)
    {
        list.add(button);
    }

    @Override
    public void remove(Component button)
    {
        list.remove(button);
    }

    @Override
    public void notifyMediator(Component button)
    {
        notifyAllClients(button);
    }
}
