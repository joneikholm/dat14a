package patterns.mediator;

/**
 * Created by joneikholm on 08-12-15.
 */
public interface Mediator
{
    public void notifyAllClients(Component button);
    public void add(Component button);
    public void remove(Component button);
    public void notifyMediator(Component button);

}
