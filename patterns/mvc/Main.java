package patterns.mvc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        IController controller = new ControllerImproved(new ModelImproved());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sampleImproved.fxml"));
        fxmlLoader.setController(controller);
        Parent root = fxmlLoader.load();

        primaryStage.setTitle("Simple MVC demo");
        primaryStage.setScene(new Scene(root, 300, 275));
        controller.initializeController();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
