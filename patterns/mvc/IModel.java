package patterns.mvc;

/**
 * Created by joneikholm on 11-12-15.
 */
public interface IModel
{

    public boolean isValidUser(String usrName, String pswrd);
}
