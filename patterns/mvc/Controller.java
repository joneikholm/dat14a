package patterns.mvc;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller implements IController {

    private IModel model;

    public Controller(IModel model){
       this.model=model;
    }


    @FXML
    private TextField usernameField;

    @FXML
    private Button loginButton;

    @FXML
    private TextField passwordField;

    @Override
    public void loginBtnHandler()
    {
        if(model.isValidUser(usernameField.getText(),
                passwordField.getText())){
            // bruger OK
            System.out.println("OK");
        }else {
            System.out.println("Not OK");
            // ukendt bruger
        }

    }

    @Override
    public void initializeController()
    {
        // not needed in this simple Controller
    }
}
