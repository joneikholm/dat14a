package patterns.mvc;

/**
 * Created by joneikholm on 11-12-15.
 */
public interface IController
{


    public void loginBtnHandler();

    public void initializeController();

}
