package patterns.mvc;

import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ControllerImproved implements IController {

    private IModel model;

    public ControllerImproved(IModel model){

        this.model=model;

    }

    public void initializeController(){
        colorPicker.setOnAction(event -> {
            Color color = colorPicker.getValue();
            String colorStr = "#"+color.toString().substring(2,8);
            leftVBox.setStyle("-fx-background-color: "+colorStr);
        });
    }

    @FXML
    private VBox leftVBox;

    @FXML
    private TextField usernameField;

    @FXML
    private TextField passwordField;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Label responseLabel;

    @Override
    public void loginBtnHandler()
    {
        if(model.isValidUser(usernameField.getText(), passwordField.getText())){
            responseLabel.setText("OK, Indeed");
        }else {
            responseLabel.setText("Not OK at all");
        }

    }
}
