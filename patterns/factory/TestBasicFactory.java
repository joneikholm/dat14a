package patterns.factory;


public class TestBasicFactory
{
  public static void main(String args[])
  {
    SimpleFactory factory;

    factory = new SimpleFactory("Oracle");

    Connection connection = factory.createConnection();

    System.out.println("You're connecting with " + 
      connection.description());
  }
}
