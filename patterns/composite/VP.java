package patterns.composite;

/**
 * Created by joneikholm on 04-12-15.
 */
public class VP extends Corporate
{
    String name, division;

    public VP(String name, String division)
    {
        this.name = name;
        this.division = division;
    }

    public void print(){
        System.out.println("Name: "+name + " div:"+division);
    }
}
