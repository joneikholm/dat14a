package patterns.composite;

import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class CorporateIterator implements Iterator<Corporate>
{
    private Corporate[] vps;
    private int location=0;

    public CorporateIterator(Corporate[] vps)
    {
       this.vps = vps;
    }

    @Override
    public boolean hasNext()// er du klar at returnere én mere
    {
        if(location<vps.length && vps[location]!=null){
            return true;
        }
        return false;
    }

    @Override
    public Corporate next()
    {

        return vps[location++];
    }
}
