package patterns.composite;

import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class Division extends Corporate
{
    private Corporate[] vps = new Corporate[100];
    private String divisionName;
    private int position=0;

    public Division(String divisionName)
    {
        this.divisionName = divisionName;
    }

    public void add(Corporate corporate){
        vps[position++] = corporate;
    }
    public Iterator<Corporate> iterator(){
        return new CorporateIterator(vps);
    }

    public void print(){
        Iterator<Corporate> iterator = iterator();
        while (iterator.hasNext()){
            iterator.next().print();
        }

    }
}
