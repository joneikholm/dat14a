package patterns.composite;

/**
 * Created by joneikholm on 04-12-15.
 */
public class TestComposite
{
    public static void main(String[] args)
    {
        Division salesDivision = new Division("Sales");
        Division westernSalesDivision = new Division("Western Sales");
        salesDivision.add(westernSalesDivision);
        Division marketingDivision = new Division("Marketing");
        Division rdDivision = new Division("R & D");
        salesDivision.add(new VP("Ole", "Sales"));
        salesDivision.add(new VP("Per", "Sales"));
        salesDivision.add(new VP("Olevina", "Sales"));
        westernSalesDivision.add(new VP("Mr. Brown", "Western Sales"));
        marketingDivision.add(new VP("Signe", "Marketing"));
        marketingDivision.add(new VP("Sigurd", "Marketing"));
        rdDivision.add(new VP("Mick", "R & D"));
        rdDivision.add(new VP("Asger", "R & D"));
        Corporation corporation = new Corporation();
        corporation.add(salesDivision);
        corporation.add(marketingDivision);
        corporation.add(rdDivision);
        corporation.add(new VP("Zuckerberg", "StandAlone"));
        corporation.print();
    }
}
