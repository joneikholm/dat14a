package patterns.composite;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by joneikholm on 04-12-15.
 */
public class Corporation extends Corporate
{
    private ArrayList<Corporate> corporates = new ArrayList<>();

    public void add(Corporate corporate){
        corporates.add(corporate);
    }

    public void print(){
        Iterator<Corporate> iterator = corporates.iterator();
        while (iterator.hasNext()){
            iterator.next().print();
        }

    }
}
