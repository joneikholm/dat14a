package patterns.flyweight;

/**
 * Created by joneikholm on 01-12-15.
 */
public class TestFlyweight
{
            // simulerer database resultat
    String[] names = {"per", "olga", "anne", "jesper"};
    int[] ids = {3,5,6,7};
    int[] scores = {66,44,88,75};
    double total = 0;

    public static void main(String[] args)
    {
        new TestFlyweight();
    }
    public TestFlyweight(){
        for (int i = 0; i <scores.length ; i++)
        {
            total += scores[i];
        }

        double average = total / scores.length;
        Student generalStudent = new Student(average);

        for (int i = 0; i < scores.length; i++)
        {
            // udskrive alle Student objekter
            generalStudent.setIt(ids[i]);
            generalStudent.setName(names[i]);
            generalStudent.setScore(scores[i]);
            System.out.println(generalStudent);
        }
    }
}
