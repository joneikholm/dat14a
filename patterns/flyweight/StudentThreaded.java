package patterns.flyweight;

/**
 * Created by joneikholm on 01-12-15.
 */
public class StudentThreaded
{
    String name;
    int it, score;
    double averageScore;
    private final static StudentThreaded singleObject = new StudentThreaded();

    private StudentThreaded(){}
    private StudentThreaded(double averageScore)
    {
        this.averageScore = averageScore;
    }

    public static StudentThreaded getInstance(){
        return singleObject;
    }

  /*  public StudentThreaded getObject(){
        if(singleObject==null){
            // CPU: stop
            singleObject=new StudentThreaded();
        }
        return singleObject;
    }*/


    public double getStanding(){
        return (((double)score)/averageScore-1.0)*100.0;
    }

    public String toString(){
        return "Name: "+name + " id: "+it +" score:"+score + " standing: "+getStanding();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getIt()
    {
        return it;
    }

    public void setIt(int it)
    {
        this.it = it;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    public double getAverageScore()
    {
        return averageScore;
    }

    public void setAverageScore(double averageScore)
    {
        this.averageScore = averageScore;
    }
}
