package patterns.observer;

import java.util.Vector;

public class Database implements Subject
{

	private Vector<Observer> observers;
	private String operation;
	private String record;
	
	
	public Database()
	{
		observers=new Vector<>();
	}

	public void editRecord(String operation, String record){  // simulerer en aktivitet
		this.operation = operation;
		this.record = record;
		notifyObservers();
	}
	
	@Override
	public void notifyObservers()
	{
		for (Observer observer : observers)
		{
			observer.update(operation, record);
		}
	}
	@Override
	public void registerObserver(Observer o)
	{
		observers.add(o);
	}
	@Override
	public void removeObserver(Observer o)
	{
		observers.remove(o);
	}

}
