package patterns.observer;

public class Boss implements Observer
{

	@Override
	public void update(String operation, String record)
	{
		System.out.println("Boss says: operation:"+operation +
				" was performed on record: "+record);
	}

}
