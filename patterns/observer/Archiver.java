package patterns.observer;

public class Archiver implements Observer
{

	@Override
	public void update(String operation, String record)
	{
		System.out.println("Archiver says: operation:"+operation +
				" was performed on record: "+record);
	}
}
