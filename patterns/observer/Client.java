package patterns.observer;

public class Client implements Observer
{

	@Override
	public void update(String operation, String record)
	{
		System.out.println("Client says: operation:"+operation +
				" was performed on record: "+record);
	}

}
