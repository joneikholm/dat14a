package adt;

/**
 * Created by joneikholm on 10-11-15.
 */
public class Entry
{
    String key;
    Object value;
    public Entry(String key, Object object){
        this.key=key;
        this.value=object;
    }
}
