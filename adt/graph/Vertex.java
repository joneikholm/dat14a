package adt.graph;

public class Vertex implements Comparable<Vertex>
{


    String name;
    public int lengthFromStart=Integer.MAX_VALUE;
    int x,y;

    public Vertex(String name, int x, int y)
    {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public Vertex(String name){

        this.name=name;
    }
 /*   public Vertex(String name, int x, int y){
        this.x=x;
        this.y=y;
        this.name=name;
    }*/


    public String toString(){
        return "Name: " + name +" length:"+lengthFromStart ;
    }


    @Override
    public int compareTo(Vertex o)
    {
        if(o.lengthFromStart == this.lengthFromStart){
            //System.out.println("Compared showed EQUAL");
            return 0;
        }
        if(this.lengthFromStart > o.lengthFromStart){
       // System.out.println("THIS er større. med: " + o.name + " length: "+o.lengthFromStart + " this:"+name + " this Length:"+lengthFromStart);
            return 1;
        }else{
      //  System.out.println("THIS er mindre. med: " + o.name + " length: "+o.lengthFromStart + " this:"+name + " this Length:"+lengthFromStart);
         return -1;
        }


    }
}