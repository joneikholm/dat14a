package adt.graph;

/**
 * Created by joneikholm on 24-11-15.
 */
public class Edge
{
    Vertex from, to;
    int weight;

    public Edge(Vertex from, Vertex to, int weight)
    {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

}
