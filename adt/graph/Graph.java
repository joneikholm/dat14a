package adt.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by joneikholm on 17-11-15.
 */
public class Graph
{
  private ArrayList<Map<Integer, Integer>> adjList = new ArrayList<>();
  private boolean[] hasBeenVisited;
    public Graph(int numberOfVertexes){
        // "starte op"
      hasBeenVisited = new boolean[numberOfVertexes];
      for (int i = 0; i <numberOfVertexes ; i++)
      {
        adjList.add(new HashMap<Integer, Integer>());
      }

    }

  public void dfs(Integer startVertex){
    hasBeenVisited[startVertex] = true;
    System.out.println("Vi har besøgt :"+startVertex);
    // implementér denne metode, som besøger ALLE vertexes
    // som er tilgængelige fra startVertex.
    // Sørg for, kun at besøge hver vertex EN gang.
    Set<Integer> set=adjList.get(startVertex).keySet();
    for (Integer integer : set) // her har vi alle naboer for startVertex
    {
      if(hasBeenVisited[integer]==false){
        dfs(integer);
      }

    }

  }

    public void addEdge(Integer v1, Integer v2, int weight){
        // implementér selv
      adjList.get(v1).put(v2, weight);
      //adjList.get(v2).put(v1, weight);  //un-directed
    }


}
