package adt.graph;

import java.util.PriorityQueue;

/**
 * Created by joneikholm on 20-11-15.
 */
public class Demo
{
    public static void main(String[] args)
    {
        PriorityQueue<Vertex> priorityQueue = new PriorityQueue<>();
        Vertex vertex1 = new Vertex("v1");
        vertex1.lengthFromStart=4;
        Vertex vertex2 = new Vertex("v2");
        vertex2.lengthFromStart=2;
        Vertex vertex3 = new Vertex("v3");
        vertex3.lengthFromStart=1;

        priorityQueue.offer(vertex1);
        priorityQueue.add(vertex2);
        priorityQueue.add(vertex3);
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
    }
}
