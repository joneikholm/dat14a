package adt.graph;

import java.util.ArrayList;

/**
 * Created by joneikholm on 17-11-15.
 */
public class UseGraph
{

    public static void main(String[] args)
    {
        new UseGraph();
    }
    public UseGraph(){
        ObjectGraph objectGraph = new ObjectGraph(9);

        Vertex v0 = new Vertex("0");
        Vertex v1 = new Vertex("1");
        Vertex v2 = new Vertex("2");
        Vertex v3 = new Vertex("3");
        Vertex v4 = new Vertex("4");
        Vertex v5 = new Vertex("5");
        Vertex v6 = new Vertex("6");
        Vertex v7 = new Vertex("7");
        Vertex v8 = new Vertex("8");
        objectGraph.addEdge(v0, v2, 7);
        objectGraph.addEdge(v0, v5, 1);
        objectGraph.addEdge(v1, v6, 2);
        objectGraph.addEdge(v2, v6, 2);
        objectGraph.addEdge(v3, v4, 2);
        objectGraph.addEdge(v4, v5, 1);
        objectGraph.addEdge(v5, v3, 1);
        objectGraph.addEdge(v5, v7, 2);
        objectGraph.addEdge(v7, v8, 1);
        objectGraph.addEdge(v7, v2, 1);

        ArrayList<Edge> edges = objectGraph.createEdges();

        System.out.println("Size of Edges Arraylist:"+edges.size());
        System.out.println(edges);
        objectGraph.dijkstra(v0,v6);

    }
}
