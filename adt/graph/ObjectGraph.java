package adt.graph;

import java.util.*;

/**
 * Created by joneikholm on 17-11-15.
 */
public class ObjectGraph
{
    //private ArrayList<Map<Integer, Integer>> adjList = new ArrayList<>();
    private HashMap<Vertex, HashMap<Vertex, Integer>> adjList = new HashMap<>();

    // Kø som sorterer Vertex. Giver altid "billigste" vertex først.
    private PriorityQueue<Vertex> priorityQueue = new PriorityQueue<>();

    //private boolean[] hasBeenVisited;
    private HashMap<Vertex, Boolean> hasBeenVisited = new HashMap<>();

    public ObjectGraph(int numberOfVertexes)
    {

    }

    public ArrayList<Edge> createEdges(){
        ArrayList<Edge> edges = new ArrayList<>();
        for (Map.Entry<Vertex, HashMap<Vertex, Integer>> entry : adjList.entrySet())
        {
            for (Map.Entry<Vertex, Integer> vertexEntry : entry.getValue().entrySet())
            {
                    Edge edge = new Edge(entry.getKey(), vertexEntry.getKey(), vertexEntry.getValue());
                    edges.add(edge);
            }
        }
        return edges;
    }

    public ArrayList<Edge> dijkstra(Vertex startVertex, Vertex endVertex)
    {

        // Map til at gemme vejen tilbage.
        HashMap<Vertex, Vertex> backTrace = new HashMap<>();
        priorityQueue.add(startVertex);  // tilføje startVertex,som vi VED skal besøges
        startVertex.lengthFromStart = 0; // sæt lengthFromStart til 0, fordi den står på Integer.MAX_VALUE
        while (!priorityQueue.isEmpty())
        {
            Vertex vertex = priorityQueue.poll(); // hvilken ? g, e, n,

            for (Map.Entry<Vertex, Integer> entry : adjList.get(vertex).entrySet())
            {

                // lav en if sætning, som løser punkt 4
                int nyLængde = vertex.lengthFromStart + entry.getValue(); // 0+24
                if (entry.getKey().lengthFromStart > nyLængde)
                {
                    entry.getKey().lengthFromStart = nyLængde;  // 24
                    priorityQueue.add(entry.getKey()); // tilføj g,n,e
                                   // g,n,e        // f
                    backTrace.put(entry.getKey(), vertex);  //gem vejen tilbage  ( her: g -> f )
                }
            }
        }

        ArrayList<Edge> pathOfEdges = new ArrayList<>();
        Vertex vertex = endVertex;
        while(vertex != null){
            if(backTrace.get(vertex)!=null){
                pathOfEdges.add(new Edge(vertex,backTrace.get(vertex), 1 ));
                System.out.println("From: "+vertex.name + " to: "+backTrace.get(vertex).name);
                vertex = backTrace.get(vertex); // den gode linie
            }else{
                break;
            }
        }
        return pathOfEdges;
        // 1. lav en løkke som kører så længe det giver mening DONE
        // 2. i løkken (1) besøges en vertex ad gangen DONE
        // 3. lav en lignende løkke som den i DFS
        // 4. sammenlign nu den totale længde som naboX har med
        //  vertex fra (2) + distancen fra vertex (2) til naboX
        // 5. Hvis naboX's eksisterende længde er større, overskriv så denne
        // med sum fra (4). Dertil tilføjes naboX til køen.


    }

    public void dfs(Vertex startVertex)
    {
        hasBeenVisited.put(startVertex, true); //hasBeenVisited[startVertex] = true;
        System.out.println("Vi har besøgt :" + startVertex);
        // implementér denne metode, som besøger ALLE vertexes
        // som er tilgængelige fra startVertex.
        // Sørg for, kun at besøge hver vertex EN gang.
        Set<Vertex> set = adjList.get(startVertex).keySet();
        for (Vertex vertex : set) // her har vi alle naboer for startVertex
        {
            if (!hasBeenVisited.get(vertex))
            {
                dfs(vertex);
            }

        }

    }


    public void addEdge(Vertex v1, Vertex v2, Integer weight)
    {
        // implementér selv
        if (adjList.get(v1) == null)
        {  // hvis det første gang vi tilføjer en Edge her
            adjList.put(v1, new HashMap<>());
            hasBeenVisited.put(v1, false); // husk dette, ellers null pointer error...
        }
        if (adjList.get(v2) == null)
        {  // hvis det første gang vi tilføjer en Edge her
            adjList.put(v2, new HashMap<>());
            hasBeenVisited.put(v2, false);
        }
        adjList.get(v1).put(v2, weight);
        // adjList.get(v1).put(v2, weight);
        //adjList.get(v2).put(v1, weight);  //un-directed
    }


}
