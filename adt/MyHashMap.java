package adt;

/**
 * Created by joneikholm on 10-11-15.
 */
public class MyHashMap
{
    private static Entry[] array=new Entry[101];
    public static void main(String[] args)
    {
        System.out.println("Java".hashCode());
        put("+45 12341234", "Søvej 45");
        put("Java", "HC Andersensvej 23");
        put("x", "Absalonsgade 99");
    }

    public static int hashcode(String s){
        int hash=0;
        int g=31;
        for (int i = 0; i <s.length() ; i++)
        {
            hash = hash*g + s.charAt(i);
        }
        return hash;
    }

    public static void put(String key, String value){
        int hashcode = hashcode(key);
        int hashcodeindex = hashcode % array.length;  // LÆNGDEN på array SKAL være primtal
        array[hashcodeindex]= new Entry(key,value);
        // udbyg denne metode, så den også kan håndere
        // linear probing
    }

}
