package adt;

public class BinaryTree
{
    private Node root;
    // 1. lav selv metodesignaturer til de
    // metoder som vi besluttede
    public void add(int item){

        root=add(root, item);
        //1. tag højde for om træet er tomt
        //2. find den korrekte plads
        //3. sørg for at HELE træet nu "hænger"
        // fast i root
    }

    private Node add(Node node, int item){
        if(node == null){
            node = new Node(item);
        }else if(item < node.item){ // vi skal vælge venstre
            node.left = add(node.left, item);
        } else {
            node.right = add(node.right, item);
        }
        return node;
    }


    public boolean contains(int item){

        return contains(root, item);
    }

    private boolean contains(Node node, int item){
        if(node == null){ // like
            return false;
        }else {
            if(node.item == item){
                return true;
            } else if(item < node.item){
                return contains(node.left, item);
            } else {
                return contains(node.right, item);
            }
        }
    }

    //2. implementér add() metoden !

    public void printTheTree(){
        System.out.println(toString(root));
    }

    public static String toString(Node root)
    {
        String result = "";
        if (root == null)
            return "";
        result += toString(root.left)+" ";
        result += toString(root.right)+" ";
        result += root.item+ " ";
        return result;
    }
}
