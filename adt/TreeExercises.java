package adt;

/**
 * Created by joneikholm on 14-12-15.
 */
public class TreeExercises
{
    public static void main(String[] args)
    {

        Node one = new Node(null,null,1 );
        Node four = new Node(null,null,4 );
        Node six = new Node(null,null,6 );
        Node two = new Node(four,six,2 );
        Node five = new Node(one,null,5 );
        Node root = new Node( five,two, 3);

        System.out.println(countLeftNodes(root));

    }

    public static int countLeftNodes(Node node){
        if (node.left == null && node.right == null){
            return 0;
        } else {
            int sum = 0;
            if (node.left != null) {
                sum = 1 + countLeftNodes(node.left);
            } if(node.right != null) {
                sum = sum + countLeftNodes(node.right);
            }
            return sum;
        }
    }
/*
    public static int countLeftNodes(Node node){
        // 1. base case
        if(node == null){
            return 0;
        }else {
        // 2. recursive case

            int sum = 1 + countLeftNodes(node.left);
               sum += countLeftNodes(node.right);
         return sum;
        }
    }*/



    public static int countLeftNodes(int number){
        if(number<100000){
            return countLeftNodes(number+1);
        }
        else return number;

    }
}
