package adt;

/**
 * Created by joneikholm on 06-11-15.
 */
public class RunCode
{
    public static void main(String[] args)
    {
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.add(23);
        binaryTree.add(5);
        binaryTree.add(2);
        binaryTree.printTheTree();
        if(binaryTree.contains(4)){
            System.out.println("indeholder 2");
        }else{
            System.out.println("indeholder ikke 2");
        }
    }
}
