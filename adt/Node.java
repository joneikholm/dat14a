package adt;

/**
 * Created by joneikholm on 06-11-15.
 */
public class Node
{
    Node left;
    Node right;
    int item;

    public Node(Node left, Node right, int item)
    {
        this.left = left;
        this.right = right;
        this.item = item;
    }

    public Node(int item){

        this.item=item;

    }
}
