package adt;

import java.util.LinkedList;

/**
 * Created by joneikholm on 10-11-15.
 */
public class HashMapSeperateChaining
{
    private static LinkedList<Entry>[] array = new LinkedList[101];

    public static void main(String[] args)
    {
        System.out.println("Java".hashCode());
        put("+45 12341234", "Søvej 45");
        put("Java", "HC Andersensvej 23");
        put("x", "Absalonsgade 99");
    }

    public static int hashcode(String s)
    {
        int hash = 0;
        int g = 31;
        for (int i = 0; i < s.length(); i++)
        {
            hash = hash * g + s.charAt(i);
        }
        return hash;
    }

    public static void put(String key, String value)
    {
        int hashcode = hashcode(key);
        int hashcodeindex = hashcode % array.length;
        Entry entry = new Entry(key, value);
        if (array[hashcodeindex] == null)
        {
            array[hashcodeindex] = new LinkedList<Entry>();
            array[hashcodeindex].add(entry);
        } else
        {
            array[hashcodeindex].add(entry);
        }
    }

    // lav selv get metoden, som tager imod en key og returnerer det
    // tilhørende objekt, hvis det findes.

}
