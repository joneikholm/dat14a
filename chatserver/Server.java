package chatserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by joneikholm on 13-11-15.
 */
public class Server
{

    private HashMap<Socket, PrintWriter> clients = new HashMap<>();
    public static void main(String[] args)
    {
        new Server();
    }
    public Server(){
        try
        {
            ServerSocket serverSocket = new ServerSocket(6780);  //den skal ikke ændres

            while(true)
            {
                System.out.println("Venter på klient...");
                Socket socket = serverSocket.accept(); // blokerer
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                clients.put(socket, printWriter);
                // Lav en KlientHandler objekt
                ClientHandler clientHandler = new ClientHandler(this, socket);
                Thread thread = new Thread(clientHandler);
                thread.start();
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public void broadcastToClients(String message)
    {
        // hjemme arbejde !!
    }

    public void logOff(Socket socket)
    {
        try
        {
            socket.close();
            clients.remove(socket);
            System.out.println(socket.getInetAddress().getHostName()+" har logget af.");
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}
