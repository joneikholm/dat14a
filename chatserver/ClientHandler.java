package chatserver;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by joneikholm on 13-11-15.
 */
public class ClientHandler implements Runnable
{
    private Server server;
    private Socket socket;

    public ClientHandler(Server server, Socket socket){
        this.server=server;
        this.socket=socket;
    }

    @Override
    public void run()
    {
        try
        {
            Scanner scanner = new Scanner(socket.getInputStream());
            while (true){
                String message=scanner.nextLine(); // blokerer !

                if(message.equalsIgnoreCase("quit")){
                    server.logOff(socket);
                    break;
                }
                server.broadcastToClients(message);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
