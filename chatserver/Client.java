package chatserver;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by joneikholm on 13-11-15.
 */
public class Client
{
    public static void main(String[] args)
    {
        new Client();
    }
    public Client(){
            try
            {
                Socket socket = new Socket(InetAddress.getLocalHost().getHostAddress(), 6780);
                OutputStream outputStream = socket.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream, true);
                printWriter.println("hello from client");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
    }
}
